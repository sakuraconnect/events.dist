var DB, MongoDB, _, collection, contactOptions, dbConf, installDefaults, loadTags, oeEventFBId, orgRoles, yesno;

dbConf = require("config").get("db-settings");

MongoDB = require("mongojs");

_ = require("underscore");

yesno = require("yesno");

DB = MongoDB(dbConf.database);

collection = DB.collection(dbConf.tables.options);

contactOptions = ["Twitter", "Instagram", "Website", "Email"];

orgRoles = ["organizer", "supporting", "sponsor"];

loadTags = ['cosplay', 'figma', 'anime', 'japan pop-culture', 'japan culture', 'manga', 'korean culture', 'korean pop-culture', 'charity', 'gaming', 'local comics', 'doujin', 'book event', 'movie event', 'esports'];

oeEventFBId = "435500213215093";


/*
  Check if the Collection is set
 */

DB.getCollectionNames(function(err, doc) {
  var isOptions;
  if (err) {
    console.log(err);
  }
  isOptions = _.contains(doc, dbConf.tables.options);
  if (!isOptions) {
    installDefaults();
    return process.exit();
  } else {
    return yesno.ask("Current settings will be overwritten!!! (Y/N)", false, function(answer) {
      if (answer) {
        return collection.remove({}, function(err, doc) {
          console.log("Options remove");
          installDefaults();
          return process.exit();
        });
      } else {
        console.log("Settings not modified");
        return process.exit();
      }
    });
  }
});

installDefaults = function() {
  var bulk;
  bulk = collection.initializeOrderedBulkOp();
  bulk.insert({
    option: 'otaku-events-id',
    value: oeEventFBId
  });
  bulk.insert({
    option: 'contacts',
    value: contactOptions
  });
  bulk.insert({
    option: 'org-roles',
    value: orgRoles
  });
  bulk.insert({
    option: "tags",
    value: loadTags
  });
  return bulk.execute(function(err, res) {});
};
