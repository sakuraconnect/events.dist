var DB, MongoDB, UsersDB, _, dbConf;

MongoDB = require("mongojs");

dbConf = require("config").get("db-settings");

_ = require("underscore");

DB = MongoDB(dbConf.database);

UsersDB = DB.collection(dbConf.tables.users);

module.exports = {
  listUsers: function(cb) {
    var _self;
    _self = this;
    return UsersDB.find({}, function(err, doc) {
      var response;
      response = {};
      if (err) {
        response.state = "Error";
        response.payload = err;
        return cb(response);
      } else {
        response.state = "Ok";
        response.payload = _self._formatPayload(doc);
        return cb(response);
      }
    });
  },
  changeRole: function(data, cb) {
    var _self, query, update;
    _self = this;
    query = {
      fbid: data.fbid
    };
    update = {
      "$set": {
        role: data.role.role
      }
    };
    return UsersDB.update(query, update);
  },
  deleteUser: function(data, cb) {
    var _self, query;
    _self = this;
    query = {
      fbid: data.fbid
    };
    return UsersDB.remove(query, true, function(err, doc) {
      var response;
      response = {};
      if (err) {
        response.state = "Error";
        response.payload = err;
        return cb(response);
      } else {
        response.payload = doc;
        return cb(response);
      }
    });
  },
  edit: function(userInfo, cb) {
    var _self, data, query;
    console.log(userInfo);
    _self = this;
    query = {
      fbid: userInfo.fbid
    };
    data = {
      '$set': {
        email: userInfo.newEmail
      }
    };
    return UsersDB.update(query, data, function(err, doc) {
      return cb(err, doc);
    });
  },
  _formatPayload: function(payload) {
    var _self;
    _self = this;
    payload = _.each(payload, function(item) {
      var role;
      role = item.role;
      return item.role = {
        role: role
      };
    });
    return payload;
  }
};
