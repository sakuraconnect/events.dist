var DB, MongoDB, _, collection, dbConf, del, moment, path, photoMod;

MongoDB = require("mongojs");

_ = require("underscore");

path = require("path");

del = require("del");

dbConf = require("config").get("db-settings");

DB = MongoDB(dbConf.database);

collection = DB.collection(dbConf.tables.events);

photoMod = require("./photos.js");

moment = require("moment");

module.exports = {
  convertPricetoInt: function(price) {
    var _self;
    _self = this;
    return parseInt(price);
  },
  formatDate: function(schedule) {
    var _self, dateString, normalizeDate, normalizeDateTime;
    _self = this;
    normalizeDate = moment(schedule.date, "DD.MM.YYYY");
    dateString = normalizeDate.format("YYYY-MM-DD") + " " + schedule.time + " " + schedule.zone;
    normalizeDateTime = moment(dateString, "YYYY-MM-DD HH:mm Z");
    return new Date(normalizeDateTime.toISOString());
  },
  dbCB: function(err, doc, cb) {
    var _self;
    _self = this;
    if (err) {
      return console.log(err);
    } else {
      return cb(doc);
    }
  },
  contacts: function(data) {
    var _self;
    _self = this;
    _.each(data.contacts, function(val, key, list) {
      if (val.value.trim().length === 0) {
        return data.contacts.splice(key, 1);
      }
    });
    return data;
  },
  tickets: function(data) {
    var _self;
    _self = this;
    _.each(data.tickets, function(val, key, list) {
      if (val.note.length > 0) {
        val.price = _self.convertPricetoInt(val.price);
        return data.tickets[key].price = val.price;
      } else {
        return data.tickets.splice(key, 1);
      }
    });
    return data;
  },
  schedule: function(data) {
    var _self, eventDates;
    _self = this;
    eventDates = data.schedule;
    delete data.schedule;
    data.schedule = {
      from: _self.formatDate(eventDates.from),
      to: _self.formatDate(eventDates.to)
    };
    return data;
  },
  add: function(data, cb) {
    var _self, addCB;
    _self = this;
    addCB = function(data) {
      data = _self.tickets(data);
      data = _self.schedule(data);
      data = _self.contacts(data);
      delete data.posterUrl;
      return collection.insert(data, function(err, doc) {
        return _self.dbCB(err, doc, cb);
      });
    };
    if (data.posterUrl != null) {
      return photoMod.resize(data.posterUrl, function(filePath) {
        data.posterFiles = {
          small: filePath.small,
          medium: filePath.medium,
          large: filePath.large,
          date: filePath.path
        };
        return addCB(data);
      });
    } else {
      return addCB(data);
    }
  },
  edit: function(data, cb) {
    var _self, types, updateCB;
    _self = this;
    data = _self.tickets(data);
    data = _self.schedule(data);
    data = _self.contacts(data);
    updateCB = function(data) {
      var query;
      if (data.orgPosterUrl != null) {
        delete data.orgPosterUrl;
      }
      delete data.posterUrl;
      query = {
        _id: MongoDB.ObjectId(data._id)
      };
      delete data._id;
      return collection.update(query, data, function(err, doc) {
        return _self.dbCB(err, doc, cb);
      });
    };
    if (data.posterUrl === !data.orgPosterUrl && (data.orgPosterUrl == null) && (data.posterUrl != null)) {
      if (data.orgPosterUrl != null) {
        types = ['small', 'medium', 'large'];
        _.each(types, function(val) {
          var filePath;
          filePath = path.join(data.posterFiles, data.posterFiles[val]);
          return del(filePath);
        });
      }
      return photoMod.resize(data.posterUrl, function(filePath) {
        data.posterFiles = {
          small: filePath.small,
          medium: filePath.medium,
          large: filePath.large,
          date: filePath.path
        };
        return updateCB(data);
      });
    } else {
      return updateCB(data);
    }
  },
  list: function(proj, cb) {
    var _self;
    _self = this;
    return collection.find({}, proj, function(err, doc) {
      return _self.dbCB(err, doc, cb);
    });
  },
  listMan: function(proj, sort, cb) {
    var _self;
    _self = this;
    return collection.find({}, proj).sort(sort, function(err, doc) {
      return _self.dbCB(err, doc, cb);
    });
  },
  changeStatus: function(eventInfo, cb) {
    var _self, data, options, query;
    _self = this;
    query = {
      _id: MongoDB.ObjectId(eventInfo._id)
    };
    data = {
      '$set': {
        status: eventInfo.status
      }
    };
    options = {
      upsert: true
    };
    return collection.update(query, data, options, function(err, doc) {
      return _self.dbCB(err, doc, cb);
    });
  },
  detailManage: function(data, cb) {
    var _self, query;
    _self = this;
    query = {
      _id: MongoDB.ObjectId(data.id)
    };
    return collection.findOne(query, function(err, doc) {
      if (err) {
        return;
      }
      return cb(err, doc);
    });
  },
  index: function(proj, cb) {
    var _self;
    _self = this;
    return collection.find({}, proj, function(err, doc) {
      return cb(err, doc);
    });
  },
  detail: function(slug, cb) {
    var _self;
    _self = this;
    return collection.findOne({
      slug: slug
    }, function(err, doc) {
      return cb(err, doc);
    });
  }
};
