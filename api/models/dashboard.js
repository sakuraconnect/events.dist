var DB, MongoDB, UsersDB, dbConf;

MongoDB = require("mongojs");

dbConf = require("config").get("db-settings");

DB = MongoDB(dbConf.database);

UsersDB = DB.collection(dbConf.tables.users);

module.exports = {
  authenticate: function(data, cb) {
    var record;
    record = {
      fbid: data.id,
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      verified: data.verified,
      role: 1000,
      created_at: new Date()
    };
    return UsersDB.find({
      fbid: data.id
    }, function(err, doc) {
      var response;
      response = {};
      if (err) {
        response.state = "Error";
        response.payload = err;
        return cb(response);
      } else {
        if (doc.length === 0) {
          return UsersDB.insert(record, function(err, doc) {
            response.state = "Users Created";
            response.payload = data;
            return cb(response);
          });
        } else {
          response.state = "User Authenticated";
          response.payload = doc;
          return cb(response);
        }
      }
    });
  }
};
