var DB, MongoDB, collection, dbConf;

MongoDB = require("mongojs");

dbConf = require("config").get("db-settings");

DB = MongoDB(dbConf.database);

collection = DB.collection(dbConf.tables.organizers);

module.exports = {
  dbCB: function(err, doc, cb) {
    var _self;
    _self = this;
    if (err) {
      return console.log(err);
    } else {
      return cb(doc);
    }
  },
  add: function(data, cb) {
    var _self, options, query, update;
    _self = this;
    if (data.id != null) {
      query = {
        _id: MongoDB.ObjectId(data.id)
      };
    } else {
      query = {
        fb_page_id: data.fb_page_id
      };
    }
    update = {
      name: data.name,
      fb_page_id: data.fb_page_id,
      contacts: data.contacts,
      created_at: new Date(),
      created_by: data.created_by
    };
    options = {
      upsert: true
    };
    return collection.update(query, update, options, function(err, doc) {
      return _self.dbCB(err, doc, cb);
    });
  },
  list: function(cb) {
    var _self, select;
    _self = this;
    select = {
      name: 1,
      fb_page_id: 1,
      contacts: 1
    };
    return collection.find({}, select).sort({
      "name": 1
    }, function(err, doc) {
      return _self.dbCB(err, doc, cb);
    });
  },
  detail: function(data, cb) {
    var _self, query;
    _self = this;
    query = {
      _id: MongoDB.ObjectId(data.id)
    };
    return collection.findOne(query, function(err, doc) {
      return _self.dbCB(err, doc, cb);
    });
  }
};
