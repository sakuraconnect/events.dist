var DB, MongoDB, _, collection, dbConf;

MongoDB = require('mongojs');

dbConf = require('config').get('db-settings');

_ = require('underscore');

DB = MongoDB(dbConf.database);

collection = DB.collection(dbConf.tables.options);

module.exports = {
  list: function(option, cb) {
    var _self, proj, query;
    _self = this;
    query = {
      option: option
    };
    proj = {
      value: 1,
      _id: 0
    };
    return collection.findOne(query, proj, function(err, doc) {
      return cb(err, doc);
    });
  },
  edit: function(data, cb) {
    var _self, query;
    _self = this;
    query = {
      option: option
    };
    return collection.update(query, {
      '$set': {
        value: data.value
      }
    }, function(err, doc) {
      return cb(err, doc);
    });
  },
  editMany: function(data, cb) {
    var _self;
    _self = this;
    return _.each(data, function(val, key, list) {
      return _self.edit(val, cb);
    });
  }
};
