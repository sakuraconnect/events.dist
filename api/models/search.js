var DB, MongoDB, apiKey, dbConf, qs, request;

MongoDB = require("mongojs");

dbConf = require("config").get("db-settings");

request = require("request");

qs = require("querystring");

DB = MongoDB(dbConf.database);

apiKey = "AIzaSyB52mIz2RwwTfIrSwdw_CBVfFIGIr80cx4";

module.exports = {
  venue: function(query, cb) {
    var endpoint, params;
    params = {
      key: apiKey,
      input: query
    };
    endpoint = "https://maps.googleapis.com/maps/api/place/queryautocomplete/json?";
    return request(endpoint + qs.stringify(params), function(err, res, body) {
      if (err === null && res.statusCode === 200) {
        return cb(body);
      } else {
        return cb(null);
      }
    });
  },
  getCoords: function(query, cb) {
    var endpoint, params;
    params = {
      key: apiKey,
      placeid: query
    };
    endpoint = "https://maps.googleapis.com/maps/api/place/details/json?";
    return request(endpoint + qs.stringify(params), function(err, res, body) {
      if (err === null && res.statusCode === 200) {
        return cb(body);
      } else {
        return null;
      }
    });
  }
};
