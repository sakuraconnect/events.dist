var Imagepath, Jimp, _, fs, moment, path, photoConf, quality, randString, sizes;

Jimp = require("jimp");

randString = require("randomstring");

moment = require("moment");

fs = require("fs-extra");

path = require("path");

_ = require("underscore");

photoConf = require("config").get("photo-settings");

Imagepath = photoConf['image-folder'];

sizes = {
  small: parseInt(photoConf.small.res),
  medium: parseInt(photoConf.medium.res),
  large: parseInt(photoConf.large.res)
};

quality = photoConf.quality;

module.exports = {
  resize: function(url, cb) {
    var _self, success;
    _self = this;
    success = function(url) {
      var d, dirPath, filename, filepath, month, year;
      d = moment();
      year = d.format("YYYY");
      month = d.format("MM");
      dirPath = path.join(Imagepath, year, month);
      filename = _self.generateFile();
      filepath = {};
      return fs.mkdirs(dirPath, function(err) {
        var trueFilePath;
        trueFilePath = path.join(dirPath, 'large-' + filename);
        filepath.path = dirPath;
        url.resize(sizes.large, Jimp.AUTO).quality(quality).write(trueFilePath);
        filepath.large = 'large-' + filename;
        trueFilePath = path.join(dirPath, 'medium-' + filename);
        filepath.path = dirPath;
        url.resize(sizes.medium, Jimp.AUTO).quality(quality).write(trueFilePath);
        filepath.medium = 'medium-' + filename;
        trueFilePath = path.join(dirPath, 'small-' + filename);
        filepath.path = dirPath;
        url.resize(sizes.small, Jimp.AUTO).quality(quality).write(trueFilePath);
        filepath.small = 'small-' + filename;
        return cb(filepath);
      });
    };
    return Jimp.read(url).then(success)["catch"](function(err) {
      return console.log(err);
    });
  },
  generateFile: function() {
    var _self, string;
    _self = this;
    string = randString.generate({
      length: 10,
      readable: true,
      charset: 'alphanumeric'
    });
    return string + '.jpg';
  }
};
