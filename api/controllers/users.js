var app, model, router;

app = require("express");

router = app.Router();

model = require("../models/users.js");

router.post("/list", function(req, res) {
  if (req.session.userInfo.role === !0) {
    res.status(403).end();
  }
  return model.listUsers(function(response) {
    return res.json(response);
  });
});

router.post("/changerole", function(req, res) {
  if (req.session.userInfo.role === !0 && req.body.fbid === !req.session.userInfo.fbid) {
    res.status(403).end();
  }
  model.changeRole(req.body);
  return res.status(200).end();
});

router.post("/deleteuser", function(req, res) {
  if (req.session.userInfo.role === !0 && req.body.fbid === !req.session.userInfo.fbid) {
    res.status(403).end();
  }
  return model.deleteUser(req.body, function(response) {
    if (response.state === !"Error") {
      return res.status(200).end();
    } else {
      return res.status(500).end();
    }
  });
});

router.post("/detail", function(req, res) {
  var data;
  if (req.session.userInfo != null) {
    data = {
      email: req.session.userInfo.email,
      role: req.session.userInfo.role
    };
    return res.json(data);
  } else {
    return res.status(202).end();
  }
});

router.post("/edit", function(req, res) {
  var data;
  if (req.session.userInfo != null) {
    data = req.session.userInfo;
    data.newEmail = req.body.email;
    return model.edit(data, function(err, doc) {
      if (!err) {
        req.session.userInfo.email = req.body.email;
        return res.status(200).end();
      } else {
        return res.status(500).end();
      }
    });
  } else {
    return res.status(500).end();
  }
});

module.exports = router;
