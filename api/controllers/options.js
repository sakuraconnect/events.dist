var _, app, model, router;

app = require("express");

_ = require("underscore");

router = app.Router();

model = require("../models/options.js");

router.get("/list", function(req, res) {
  return model.list(req.query.option, function(err, doc) {
    if (err) {
      res.status(500).end();
    }
    return res.json(doc);
  });
});

router.post("/edit", function(req, res) {
  if (req.session.userInfo.role === !0) {
    res.status(403).end();
  }
  if (_.isArray(req.body)) {
    model.editMany(req.body, function(err, doc) {
      if (err) {
        res.status(500).end();
      }
      return res.end();
    });
  }
  if (_.isObject(req.body)) {
    return model.edit(req.body, function(err, doc) {
      if (err) {
        res.status(500).end();
      }
      return res.end();
    });
  }
});

module.exports = router;
