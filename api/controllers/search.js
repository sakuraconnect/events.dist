var app, model, router;

app = require("express");

router = app.Router();

model = require("../models/search.js");

router.post("/venue", function(req, res) {
  return model.venue(req.body.query, function(response) {
    if (response != null) {
      return res.send(response);
    } else {
      return res.status(500).end();
    }
  });
});

router.post("/get-place-coords", function(req, res) {
  return model.getCoords(req.body.query, function(response) {
    if (response != null) {
      return res.send(response);
    } else {
      return res.status(500).end();
    }
  });
});

module.exports = router;
