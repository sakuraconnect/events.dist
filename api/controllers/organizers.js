var app, model, router;

app = require("express");

router = app.Router();

model = require("../models/organizers.js");

router.post("/add", function(req, res) {
  var data;
  data = {
    id: req.body._id,
    name: req.body.name,
    fb_page_id: req.body.fb_page_id,
    contacts: req.body.contacts,
    created_by: req.session.userInfo.fbid
  };
  return model.add(data, function(doc) {
    if (doc.ok) {
      return res.status(200).end();
    } else {
      return res.status(500).end();
    }
  });
});

router.get("/list", function(req, res) {
  return model.list(function(data) {
    return res.json(data);
  });
});

router.post("/detail", function(req, res) {
  return model.detail(req.body, function(data) {
    return res.json(data);
  });
});

module.exports = router;
