var app, model, router;

app = require("express");

router = app.Router();

model = require("../models/authenticate.js");

router.post('/login', function(req, res) {
  return model.authenticate(req.body, function(data) {
    req.session.userInfo = data.payload;
    return res.json(data);
  });
});

router.post('/session', function(req, res) {
  if (req.session.userInfo != null) {
    return res.status(200).json(req.session.userInfo);
  } else {
    return res.status(401).end();
  }
});

router.post('/logout', function(req, res) {
  delete req.session.userInfo;
  if (req.session.userInfo == null) {
    return res.status(299).end();
  } else {
    return res.status(501).end();
  }
});

module.exports = router;
