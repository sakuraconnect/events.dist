var app, model, router;

app = require("express");

router = app.Router();

model = require("../models/events.js");

router.post("/add", function(req, res) {
  var data;
  if (req.session.userInfo.role > 500) {
    res.status(403).end();
  }
  data = req.body;
  data.created_by = req.session.userInfo.fbid;
  data.created_at = new Date();
  data.status = "draft";
  return model.add(data, function(doc) {
    return res.json(doc);
  });
});

router.post("/manage/edit", function(req, res) {
  var data;
  if (req.session.userInfo.role > 500) {
    res.status(403).end();
  }
  data = req.body;
  data.created_by = req.session.userInfo.fbid;
  data.status = "draft";
  return model.edit(data, function(doc) {
    return res.json(doc);
  });
});

router.get("/list", function(req, res) {
  var proj;
  if (req.session.userInfo.role > 500) {
    res.status(403).end();
  }
  proj = {
    oe_event_id: 1,
    fb_event_id: 1,
    name: 1,
    schedule: 1,
    posterFiles: 1
  };
  return model.list(proj, function(doc) {
    return res.json(doc);
  });
});

router.get("/manage/list", function(req, res) {
  var proj, sort;
  if (req.session.userInfo.role > 500) {
    res.status(403).end();
  }
  proj = {
    name: 1,
    posterFiles: 1,
    schedule: 1,
    status: 1,
    'loc.venue': 1
  };
  sort = {
    'schedule.from': -1
  };
  return model.listMan(proj, sort, function(doc) {
    return res.json(doc);
  });
});

router.post("/manage/change_status", function(req, res) {
  if (req.session.userInfo.role > 500) {
    res.status(403).end();
  }
  return model.changeStatus(req.body, function(err, doc) {
    return res.status(200).end();
  });
});

router.get("/manage/detail", function(req, res) {
  if (req.session.userInfo.role > 500) {
    res.status(403).end();
  }
  return model.detailManage(req.query, function(err, doc) {
    return res.json(doc);
  });
});

router.get("/index", function(req, res) {
  var proj;
  proj = {
    schedule: 1,
    loc: 1,
    name: 1,
    posterFiles: 1,
    slug: 1
  };
  return model.index(proj, function(err, doc) {
    if (err) {
      res.status(403).end();
    }
    return res.json(doc);
  });
});

router.get("/detail", function(req, res) {
  return model.detail(req.query.slug, function(err, doc) {
    if (err) {
      res.status(500).end();
    }
    return res.json(doc);
  });
});

module.exports = router;
